/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_links.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/06 16:31:33 by jvuillez          #+#    #+#             */
/*   Updated: 2018/02/06 16:31:34 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int			ft_free_lem_links(t_lm_link **start)
{
	t_lm_link	*tmp;
	t_lm_link	*to_free;

	tmp = *start;
	while (tmp != NULL)
	{
		to_free = tmp;
		tmp = tmp->next;
		free(to_free);
	}
	*start = NULL;
	return (-1);
}

void		ft_get_names_to_link(char *line, char **name_1, char **name_2)
{
	int		i;

	i = 0;
	if (line != NULL)
	{
		while (line[i] != '-')
			i++;
		if ((*name_1 = malloc(sizeof(char) * i + 2)) != NULL)
		{
			ft_strncpy(*name_1, line, i);
			(*name_1)[i] = '\0';
			line = line + i + 1;
			*name_2 = malloc(sizeof(char) * ft_strlen(line) + 1);
			ft_strcpy(*name_2, line);
		}
	}
}

void		ft_add_link(t_room *room_1, t_room *room_2, int other_way)
{
	t_lm_link	*tmp;
	t_lm_link	*link_ptr;

	link_ptr = NULL;
	if ((tmp = malloc(sizeof(t_lm_link))) != NULL)
	{
		tmp->room = room_2;
		tmp->next = NULL;
	}
	if (room_1->links == NULL)
		room_1->links = tmp;
	else
	{
		link_ptr = room_1->links;
		while (link_ptr->next != NULL)
			link_ptr = link_ptr->next;
		link_ptr->next = tmp;
	}
	if (other_way == 1)
		ft_add_link(room_2, room_1, 0);
}

int			ft_add_ptr(t_room *list, char *name_1, char *name_2)
{
	t_room	*room_ptr1;
	t_room	*room_ptr2;

	room_ptr1 = NULL;
	room_ptr2 = NULL;
	while (list != NULL)
	{
		if (ft_strcmp(name_1, list->name) == 0)
		{
			if (room_ptr1 != NULL)
				return (-1);
			room_ptr1 = list;
		}
		if (ft_strcmp(name_2, list->name) == 0)
		{
			if (room_ptr2 != NULL)
				return (-1);
			room_ptr2 = list;
		}
		list = list->next_in_list;
	}
	if (room_ptr1 == NULL || room_ptr2 == NULL)
		return (-1);
	ft_add_link(room_ptr1, room_ptr2, 1);
	return (0);
}

int			ft_create_links(t_lm_line *read, t_room *first)
{
	int		ret;
	char	*name_1;
	char	*name_2;

	name_1 = NULL;
	name_2 = NULL;
	while (read != NULL && ft_check_link(read->str) == 0)
	{
		ft_get_names_to_link(read->str, &name_1, &name_2);
		ret = ft_add_ptr(first, name_1, name_2);
		free(name_1);
		free(name_2);
		if (ret == -1)
			return (-1);
		read = read->next;
	}
	return (0);
}
