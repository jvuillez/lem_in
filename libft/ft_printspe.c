/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printspe.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/05 18:19:51 by jvuillez          #+#    #+#             */
/*   Updated: 2016/04/04 12:43:14 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libprintf.h"

void	ft_classic_print(t_format *f)
{
	f->tot += ft_putstr(f->to_print);
}

void	ft_add_prec(char first, t_format *f)
{
	f->to_print = ft_strjoin_free(\
		ft_strspam(first, f->prec - ft_strlen(f->to_print)), f->to_print, 2);
	if (f->data == 0 && (f->p == 1 && f->prec <= 0))
	{
		if ((f->flag & DIESE) == 0)
		{
			free(f->to_print);
			f->to_print = ft_strdup("");
		}
		else if (f->sp != 'o' && f->sp != 'O')
		{
			free(f->to_print);
			f->to_print = ft_strdup("");
		}
	}
	f->to_print = ft_strjoin_free(\
		f->to_print, ft_strspam(' ', -f->prec - ft_strlen(f->to_print)), 2);
}

void	ft_decrease_width(t_format *f, int how_much)
{
	while (f->width != 0 && how_much-- > 0)
	{
		if (f->width > 0)
			f->width--;
		else
			f->width++;
	}
}

void	ft_add_width(char first, char last, t_format *f)
{
	if (f->prec >= 0)
	{
		f->to_print = ft_strjoin_free(\
			ft_strspam(first, f->width - ft_strlen(f->to_print)), \
			f->to_print, 2);
		f->to_print = ft_strjoin_free(\
			f->to_print, \
			ft_strspam(last, -f->width - ft_strlen(f->to_print)), 2);
	}
}

char	ft_get_sign(t_format *f)
{
	if ((f->flag & PLUS) != 0)
		return ('+');
	if ((f->flag & SPACE) != 0)
		return (' ');
	return ('\0');
}
