/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/15 21:29:37 by jvuillez          #+#    #+#             */
/*   Updated: 2014/04/15 21:32:30 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *s1, const void *s2, int c, size_t n)
{
	unsigned char		*s1b;
	const unsigned char	*s2b;

	s1b = (unsigned char*)s1;
	s2b = (const unsigned char*)s2;
	while (n > 0)
	{
		*s1b = *s2b;
		if (*s2b == (unsigned char)c)
			return ((void*)(s1b + 1));
		s1b++;
		s2b++;
		n--;
	}
	return (NULL);
}
