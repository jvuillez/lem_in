/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strndup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/18 17:50:57 by jvuillez          #+#    #+#             */
/*   Updated: 2017/12/18 17:50:59 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strndup(const char *s1, int n)
{
	char	*s2;
	char	*sf;

	if (n < 0)
		n = 0;
	s2 = (char*)malloc(sizeof(*s2) * (n + 1));
	if (s2 == NULL)
		return (NULL);
	sf = s2;
	while (*s1 != '\0' && n > 0)
	{
		*s2 = *s1;
		s1++;
		s2++;
		n--;
	}
	*s2 = '\0';
	return (sf);
}
