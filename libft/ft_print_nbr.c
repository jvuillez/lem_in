/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_nbr.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/20 16:25:52 by jvuillez          #+#    #+#             */
/*   Updated: 2016/04/04 12:39:03 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libprintf.h"

void		ft_init_ftab_nbr(void (*tab_ft[127])(t_format *f))
{
	tab_ft['H'] = &ft_print_nbr_char;
	tab_ft['h'] = &ft_print_nbr_short;
	tab_ft['L'] = &ft_print_nbr_longlong;
	tab_ft['l'] = &ft_print_nbr_long;
	tab_ft['j'] = &ft_print_nbr_intmax_t;
	tab_ft['z'] = &ft_print_nbr_size_t;
	tab_ft['0'] = &ft_print_nbr_int;
}

char		*ft_ulltoa_base(unsigned long long nb, char *base)
{
	char	*str;
	int		len;
	int		i;

	if (nb == 0)
		return (ft_strdup("0"));
	len = ft_strlen(base);
	str = ft_strnew(ft_nb_length(nb, base));
	i = ft_nb_length(nb, base) - 1;
	while (nb)
	{
		str[i] = base[nb % len];
		nb = nb / len;
		i--;
	}
	return (str);
}

/*
**	ah ! si j'avais pu la mettre avec les autres dans ft_print_nbr_x.c
*/

void		ft_print_nbr_p(t_format *f)
{
	unsigned long	nb;
	static char		*base = "0123456789abcdef";
	char			to_spam;

	to_spam = ' ';
	if ((f->flag & ZERO) != 0 && f->p == 0)
		to_spam = '0';
	nb = (unsigned long)f->data;
	if (to_spam == '0')
		ft_decrease_width(f, 2);
	if (f->p == 1 && f->prec == 0 && nb == 0)
		f->to_print = f->to_print;
	else
		f->to_print = ft_strjoin_free(f->to_print, ft_ulltoa_base(nb, base), 2);
	if (f->prec < -1)
		f->prec += 2;
	else if (f->prec < 0)
		f->prec++;
	ft_add_prec('0', f);
	if (to_spam == ' ')
		f->to_print = ft_strjoin_free("0x", f->to_print, 3);
	ft_add_width(to_spam, ' ', f);
	if (to_spam == '0')
		f->to_print = ft_strjoin_free("0x", f->to_print, 3);
}
