/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_nbr_x.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/14 18:41:33 by jvuillez          #+#    #+#             */
/*   Updated: 2017/12/14 18:41:38 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libprintf.h"

void		ft_print_nbr_o(t_format *f)
{
	static void		(*tab_ft[127])(t_format *f);
	char			to_spam;

	to_spam = ' ';
	if ((f->flag & ZERO) != 0 && f->p == 0)
		to_spam = '0';
	if (f->sp == 'O')
		f->length = 'l';
	ft_init_ftab_nbr(tab_ft);
	tab_ft[f->length](f);
	if ((f->flag & DIESE) != 0 && f->data != 0)
		f->to_print = ft_strjoin_free("0", f->to_print, 3);
	ft_add_prec('0', f);
	ft_add_width(to_spam, ' ', f);
}

void		ft_print_nbr_u(t_format *f)
{
	static void		(*tab_ft[127])(t_format *f);
	char			to_spam;

	to_spam = ' ';
	if ((f->flag & ZERO) != 0 && f->p == 0)
		to_spam = '0';
	if (f->sp == 'U')
		f->length = 'l';
	ft_init_ftab_nbr(tab_ft);
	tab_ft[f->length](f);
	ft_add_prec('0', f);
	ft_add_width(to_spam, ' ', f);
}

void		ft_print_nbr_di(t_format *f)
{
	static void		(*tab_ft[127])(t_format *f);
	char			to_spam;

	to_spam = ' ';
	if ((f->flag & ZERO) != 0 && f->p == 0)
		to_spam = '0';
	f->sign = ft_get_sign(f);
	ft_init_ftab_nbr(tab_ft);
	tab_ft[f->length](f);
	if (f->prec < 0 && f->sign != '\0')
		f->prec++;
	ft_add_prec('0', f);
	if (to_spam == '0')
	{
		if (f->sign != '\0')
			ft_decrease_width(f, 1);
		ft_add_width(to_spam, ' ', f);
		f->to_print = ft_strjoin_free(ft_strspam(f->sign, 1), f->to_print, 2);
	}
	if (to_spam == ' ')
	{
		f->to_print = ft_strjoin_free(ft_strspam(f->sign, 1), f->to_print, 2);
		ft_add_width(to_spam, ' ', f);
	}
}

void		ft_print_nbr_bigx(t_format *f)
{
	static void		(*tab_ft[127])(t_format *f);
	char			to_spam;

	to_spam = ' ';
	if ((f->flag & ZERO) != 0 && f->p == 0)
		to_spam = '0';
	if (f->prec < -1 && f->data != 0 && (f->flag & DIESE) != 0)
		f->prec += 2;
	else if (f->prec < 0 && f->data != 0 && (f->flag & DIESE) != 0)
		f->prec++;
	if ((f->flag & DIESE) != 0 && f->data != 0 && to_spam == '0')
		ft_decrease_width(f, 2);
	ft_init_ftab_nbr(tab_ft);
	tab_ft[f->length](f);
	ft_add_prec('0', f);
	if ((f->flag & DIESE) != 0 && f->data != 0 && to_spam == ' ')
		f->to_print = ft_strjoin_free("0X", f->to_print, 3);
	ft_add_width(to_spam, ' ', f);
	if ((f->flag & DIESE) != 0 && f->data != 0 && to_spam == '0')
		f->to_print = ft_strjoin_free("0X", f->to_print, 3);
}

void		ft_print_nbr_x(t_format *f)
{
	static void		(*tab_ft[127])(t_format *f);
	char			to_spam;

	to_spam = ' ';
	if ((f->flag & ZERO) != 0 && f->p == 0)
		to_spam = '0';
	if (f->prec < -1 && f->data != 0 && (f->flag & DIESE) != 0)
		f->prec += 2;
	else if (f->prec < 0 && f->data != 0 && (f->flag & DIESE) != 0)
		f->prec++;
	if ((f->flag & DIESE) != 0 && f->data != 0 && to_spam == '0')
		ft_decrease_width(f, 2);
	ft_init_ftab_nbr(tab_ft);
	tab_ft[f->length](f);
	ft_add_prec('0', f);
	if ((f->flag & DIESE) != 0 && f->data != 0 && to_spam == ' ')
		f->to_print = ft_strjoin_free("0x", f->to_print, 3);
	ft_add_width(to_spam, ' ', f);
	if ((f->flag & DIESE) != 0 && f->data != 0 && to_spam == '0')
		f->to_print = ft_strjoin_free("0x", f->to_print, 3);
}
