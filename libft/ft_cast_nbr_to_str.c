/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cast_nbr_to_str.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 16:55:53 by jvuillez          #+#    #+#             */
/*   Updated: 2018/01/23 16:55:55 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libprintf.h"

void		ft_print_nbr_char(t_format *f)
{
	unsigned char	unb;
	char			nb;

	unb = (unsigned char)f->data;
	nb = (char)f->data;
	free(f->to_print);
	if (f->sp == 'd' && nb < 0)
	{
		f->sign = '-';
		f->to_print = ft_ulltoa_base(-nb, f->base);
	}
	else
		f->to_print = ft_ulltoa_base(unb, f->base);
}

void		ft_print_nbr_short(t_format *f)
{
	unsigned short	unb;
	short			nb;

	unb = (unsigned short)f->data;
	nb = (short)f->data;
	free(f->to_print);
	if (f->sp == 'd' && nb < 0)
	{
		f->sign = '-';
		f->to_print = ft_ulltoa_base(-nb, f->base);
	}
	else
		f->to_print = ft_ulltoa_base(unb, f->base);
}

void		ft_print_nbr_int(t_format *f)
{
	unsigned int	unb;
	int				nb;

	unb = (unsigned int)f->data;
	nb = (int)f->data;
	free(f->to_print);
	if (f->sp == 'd' && nb < 0)
	{
		f->sign = '-';
		if (nb == -2147483648)
			f->to_print = ft_strdup("2147483648");
		else
			f->to_print = ft_ulltoa_base(-nb, f->base);
	}
	else
		f->to_print = ft_ulltoa_base(unb, f->base);
}

void		ft_print_nbr_long(t_format *f)
{
	unsigned long	unb;
	long			nb;

	unb = (unsigned long)f->data;
	nb = (long)f->data;
	free(f->to_print);
	if ((f->sp == 'd' || f->sp == 'D') && nb < 0)
	{
		f->sign = '-';
		f->to_print = ft_ulltoa_base(-nb, f->base);
	}
	else
		f->to_print = ft_ulltoa_base(unb, f->base);
}

void		ft_print_nbr_longlong(t_format *f)
{
	unsigned long long	unb;
	long long			nb;

	unb = (unsigned long long)f->data;
	nb = (long long)f->data;
	free(f->to_print);
	if (f->sp == 'd' && nb < 0)
	{
		f->sign = '-';
		f->to_print = ft_ulltoa_base(-nb, f->base);
	}
	else
		f->to_print = ft_ulltoa_base(unb, f->base);
}
