/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strspam.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/11 19:27:19 by jvuillez          #+#    #+#             */
/*   Updated: 2017/12/11 19:32:27 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strspam(char c, int i)
{
	char	*str;
	int		n;

	if (i <= 0)
	{
		str = ft_strnew(0);
		str[0] = '\0';
		return (str);
	}
	n = 0;
	str = ft_strnew(i);
	if (str == NULL)
		return (NULL);
	while (n < i)
	{
		str[n] = c;
		n++;
	}
	str[n] = '\0';
	return (str);
}
