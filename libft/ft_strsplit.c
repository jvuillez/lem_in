/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/17 19:41:29 by jvuillez          #+#    #+#             */
/*   Updated: 2014/04/18 18:48:46 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_count_words_whitespaces(const char *str)
{
	int		i;

	i = 0;
	while (*str != '\0')
	{
		if (*str != ' ' && *str != '\t')
		{
			while (*str && *str != ' ' && *str != '\t')
				str++;
			i++;
		}
		else
			str++;
	}
	return (i);
}

int		ft_count_words(const char *str, char c)
{
	int		i;

	i = 0;
	while (*str != '\0')
	{
		if (*str != c)
		{
			while (*str && *str != c)
				str++;
			i++;
		}
		else
			str++;
	}
	return (i);
}

char	**ft_strsplit(char const *s, char c)
{
	char			**tab;
	int				t;
	size_t			len;
	int				nb_words;

	nb_words = ft_count_words(s, c);
	if (!(tab = (char **)malloc(sizeof(char*) * (nb_words + 1))))
		return (NULL);
	tab[nb_words] = NULL;
	t = 0;
	while (t != nb_words)
	{
		while (*s != '\0' && *s == c)
			s++;
		len = 0;
		while (*s != '\0' && *s != c)
		{
			len++;
			s++;
		}
		tab[t] = ft_strsub((s - len), 0, len);
		t++;
	}
	return (tab);
}
