/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parser.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/14 17:05:43 by jvuillez          #+#    #+#             */
/*   Updated: 2016/04/04 12:17:15 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libprintf.h"

/*
**	%[flags][width][.precision][length]specifier
**
**	%[flags]		-	;	+	;	~space	;	#	;	0
**	[width]			~number		;	*
**	[.precision]	~.number	;	.*
**	[length]		modify the specifier
**					hh	;	h	;	l	;	ll	;	j	;	z	;	t	;	L
**		42			hh	;	h	;	l	;	ll	;	j	;	z
** specifier	% s   p d   i o   u   x X c   f F e E g G a A n
** specifier42	% s S p d D i o O u U x X c C
*/

void			ft_init_ftab(void (*tab_ft[127])(t_format *f))
{
	tab_ft['s'] = &ft_print_s;
	tab_ft['S'] = &ft_bigs;
	tab_ft['p'] = &ft_print_nbr_p;
	tab_ft['d'] = &ft_print_nbr_di;
	tab_ft['o'] = &ft_print_nbr_o;
	tab_ft['O'] = &ft_print_nbr_o;
	tab_ft['u'] = &ft_print_nbr_u;
	tab_ft['U'] = &ft_print_nbr_u;
	tab_ft['x'] = &ft_print_nbr_x;
	tab_ft['X'] = &ft_print_nbr_bigx;
	tab_ft['c'] = &ft_print_c;
	tab_ft['%'] = &ft_print_c;
	tab_ft['C'] = &ft_bigc;
}

void			ft_length_modifier(t_format *f)
{
	if (f->sp == 'i')
		f->sp = 'd';
	ft_strcpy(f->base, "0123456789\0");
	if (f->sp == 'o' || f->sp == 'O')
		ft_strcpy(f->base, "01234567\0");
	if (f->sp == 'x')
		ft_strcpy(f->base, "0123456789abcdef\0");
	if (f->sp == 'X')
		ft_strcpy(f->base, "0123456789ABCDEF\0");
	if (f->length == 'l')
	{
		if (f->sp == 'c' || f->sp == 's')
			f->sp = ft_toupper(f->sp);
	}
	if (f->sp == 'D')
	{
		f->sp = 'd';
		f->length = 'l';
	}
}

/*
**			FT_PARSER
**	recupere flags][width][.precision][length]specifier
**	grace a FT_GET_FORMAT
**
**	puis met en forme l'argument f->data dans f->to_print
**	"%+010d" avec f->data = 42 sera stocke
**	"+000000010" dans f->to_print
**	grace aux fonctions de FT_INIT_FTAB
**	(stockees dans tab_ft)
**	et/ou des fonctions comme FT_ADD_PREC, FT_ADD_WIDTH, ...
**
**	FT_LENGTH_MODIFIER modifie la base a utiliser selon le specifier
**	avant d'appeler la fonction appropriee => TAB_FT[F->SP](F);
*/

void			ft_parser(char *str, va_list ap, t_format *f)
{
	static void		(*tab_ft[127])(t_format *f);
	char			to_spam;

	str = ft_get_format(str, f, ap);
	to_spam = ' ';
	if ((f->flag & ZERO) != 0)
		to_spam = '0';
	f->ft_ptr = &ft_classic_print;
	ft_init_ftab(tab_ft);
	if (ft_specifier(f->sp) == -1)
	{
		f->to_print = ft_strjoin_free(f->to_print, ft_strdup(str), 2);
		if (f->prec < 0)
			ft_add_prec(' ', f);
		ft_add_width(to_spam, ' ', f);
	}
	else
	{
		ft_length_modifier(f);
		tab_ft[f->sp](f);
	}
}
