/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/15 23:10:26 by jvuillez          #+#    #+#             */
/*   Updated: 2014/04/18 18:53:12 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_atoi(const char *str)
{
	int	i;
	int	pn;

	i = 0;
	pn = 1;
	while ((*str != '\0') && ((*str != '-') && (*str != '+')) && (*str <= 32))
		str++;
	if (*str == '+')
		str++;
	else if (*str == '-')
	{
		pn = -1;
		str++;
	}
	else if (ft_isdigit(*str != 1))
		return (0);
	while ((*str != '\0') && (ft_isdigit(*str) == 1))
	{
		i = i * 10 + *str - 48;
		str++;
	}
	return (i * pn);
}
