/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/14 14:55:43 by jvuillez          #+#    #+#             */
/*   Updated: 2017/12/14 14:59:04 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_putnstr(char *str, int n)
{
	if (n > ft_strlen(str))
		n = ft_strlen(str) + 1;
	write(1, str, n);
	return (ft_strlen(str));
}
