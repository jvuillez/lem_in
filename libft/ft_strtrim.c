/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/17 18:15:24 by jvuillez          #+#    #+#             */
/*   Updated: 2014/04/17 19:39:28 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(char const *s)
{
	size_t	size;

	while ((*s == ' ') || (*s == '\n') || (*s == '\t'))
		s++;
	size = ft_strlen(s);
	while (size > 1
			&& (s[size - 1] == ' '
			|| s[size - 1] == '\t'
			|| s[size - 1] == '\n'))
		--size;
	return (ft_strsub(s, 0, size));
}
