/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lexer.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/27 19:34:10 by jvuillez          #+#    #+#             */
/*   Updated: 2017/12/27 19:34:12 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libprintf.h"

/*
**	%[flags][width][.precision][length]specifier
**
**	%[flags]		-	;	+	;	~space	;	#	;	0
**	[width]			~number		;	*
**	[.precision]	~.number	;	.*
**	[length]		modify the specifier
**					hh	;	h	;	l	;	ll	;	j	;	z	;	t	;	L
**		42			hh	;	h	;	l	;	ll	;	j	;	z
** specifier	% s S p d D i o O u U x X c C f F e E g G a A n
** specifier42	% s S p d D i o O u U x X c C
*/

/*
**				FT_LEXER
**	ft_lexer transforme vos (const char *str)
**	en liste chainee de type t_elem
**
**	typedef struct		s_elem{
**		struct s_format	*format;
**		char			*str;
**		struct s_elem	*next;
**	}					t_elem;
**
**	->format contiendra
**	[flags][width][.precision][length]specifier
**	== les infos utiles pour print l'argument
**
**	->str sera le morceau de chaine str
**	(jusqu'au prochain char '%'
**	ou jusqu'au prochain char specifier
**	grace a FT_SPECIFIER et FT_TRUE_CHAR)
**	grace a FT_STRLEN_NEXT
**
**	->next sera le prochain maillon
**
**	"ceci est %#10.42d ma chaine !"
**	deviendra donc
**	"ceci est "
**	"%#10.42d"
**	" ma chaine !"
*/

int				ft_true_char(char c)
{
	static char	true_chars[] = "%pdDioOuUxXcCsS#-+ *.0123456789hljz";
	int			i;

	i = 0;
	while (true_chars[i] != '\0' && c != true_chars[i])
		i++;
	if (true_chars[i] == '\0')
		return (-1);
	return (i);
}

int				ft_specifier(char c)
{
	static char	specifiers[] = "%pdDioOuUxXcCsS";
	int			i;

	i = 0;
	while (specifiers[i] != '\0' && c != specifiers[i])
		i++;
	if (specifiers[i] == '\0')
		return (-1);
	return (i);
}

unsigned int	ft_strlen_next(char *str, unsigned int i)
{
	if (i == 1)
	{
		while ((str[i] != '\0') && \
				ft_true_char(str[i]) != -1 && \
				ft_specifier(str[i]) == -1)
			i++;
		if (str[i] == '\0')
			return (i);
		else
			return (i + 1);
	}
	else
	{
		while ((str[i] != '\0') && (str[i] != '%'))
			i++;
		return (i);
	}
}

t_elem			*ft_lexer(const char *str)
{
	unsigned int	len;
	int				i;
	t_elem			*start;
	t_elem			*tmp;

	start = malloc(sizeof(t_elem));
	tmp = start;
	while (tmp != NULL && ft_strlen((char*)str) != 0)
	{
		tmp->next = malloc(sizeof(t_elem));
		if ((tmp->format = malloc(sizeof(t_format))) != NULL)
			tmp->format->to_print = NULL;
		i = 0;
		if (*str == '%')
			i = 1;
		len = ft_strlen_next((char*)str, i);
		if ((tmp->str = malloc(sizeof(char) * len + 1)) != NULL)
			ft_strnclone((char*)str, tmp->str, len);
		str += len;
		tmp = tmp->next;
	}
	tmp->next = NULL;
	return (start);
}
