/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_nb_length.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/06 14:26:32 by jvuillez          #+#    #+#             */
/*   Updated: 2017/12/06 14:27:08 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_nb_length(unsigned long long nb, char *base)
{
	int				base_len;
	unsigned int	nb_len;

	base_len = ft_strlen(base);
	nb_len = 0;
	if (nb == 0)
		nb_len = 1;
	while (nb != 0)
	{
		nb_len++;
		nb = nb / base_len;
	}
	return (nb_len);
}
