/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ustrlen.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/06 14:39:38 by jvuillez          #+#    #+#             */
/*   Updated: 2017/12/06 14:50:13 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		ft_what_the_size(wchar_t value)
{
	value = ft_size_bin(value);
	if (value > 7 && value <= 11)
		return (2);
	if (value > 11 && value <= 16)
		return (3);
	if (value > 16)
		return (4);
	return (1);
}

int				ft_ustrlen(wchar_t *str)
{
	wchar_t	i;
	int		tot;

	i = 0;
	tot = 0;
	while (str[i] != '\0')
	{
		tot += ft_what_the_size(str[i]);
		i++;
	}
	return (tot);
}
