/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/15 22:27:30 by jvuillez          #+#    #+#             */
/*   Updated: 2014/04/15 22:27:40 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t	dest_length_size;
	size_t	src_length_size;
	size_t	i;

	dest_length_size = 0;
	src_length_size = ft_strlen(src);
	while ((dst[dest_length_size] != '\0') && (dest_length_size < size))
	{
		dest_length_size++;
	}
	i = size - dest_length_size;
	if (i == 0)
		return (dest_length_size + src_length_size);
	dst = dst + dest_length_size;
	i--;
	while ((*src != '\0') && (i > 0))
	{
		*dst = *src;
		src++;
		dst++;
		i--;
	}
	*dst = '\0';
	return (dest_length_size + src_length_size);
}
