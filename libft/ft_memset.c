/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/15 21:11:32 by jvuillez          #+#    #+#             */
/*   Updated: 2014/04/15 21:15:52 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memset(void *b, int c, size_t len)
{
	unsigned char	ic;
	char			*strb;

	ic = (unsigned char)c;
	strb = (char*)b;
	while (len > 0)
	{
		*strb = ic;
		strb++;
		len--;
	}
	return (b);
}
