/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/09/23 10:50:01 by jvuillez          #+#    #+#             */
/*   Updated: 2014/09/28 23:39:04 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"
#include "libprintf.h"

int		ft_checkbuff(char *str)
{
	int		i;

	i = 0;
	if (str == NULL)
		return (0);
	while (*str != '\0')
	{
		if (*str == '\n')
			i++;
		str++;
	}
	return (i);
}

void	ft_buftoline(char *staticbuf, char **line)
{
	int		i;

	i = 0;
	(*line) = malloc(sizeof(char) * ft_strlen(staticbuf) + 1);
	while (staticbuf[i] != '\0' && staticbuf[i] != '\n')
	{
		(*line)[i] = staticbuf[i];
		i++;
	}
	(*line)[i] = '\0';
}

char	*ft_bufmanager(char *buf)
{
	char	*str;
	char	*save;
	int		i;

	i = 0;
	save = ft_strdup(buf);
	str = save;
	free(buf);
	while (*str != '\n')
		str++;
	str++;
	buf = malloc(sizeof(char) * ft_strlen(str) + 1);
	ft_strcpy(buf, str);
	free(save);
	return (buf);
}

int		get_next_line(int const fd, char **line)
{
	static char		*staticbuf = NULL;
	char			buf[BUFF_SIZE + 1];
	int				size;

	while (ft_checkbuff(staticbuf) == 0)
	{
		if ((size = read(fd, buf, BUFF_SIZE)) == -1)
			return (-1);
		if (size == 0)
		{
			if (!staticbuf || staticbuf[0] == '\0')
				return (0);
			ft_buftoline(staticbuf, line);
			staticbuf[0] = '\0';
			return (1);
		}
		buf[size] = '\0';
		staticbuf = ft_strjoin_free(staticbuf, buf, 1);
	}
	ft_buftoline(staticbuf, line);
	staticbuf = ft_bufmanager(staticbuf);
	return (1);
}
