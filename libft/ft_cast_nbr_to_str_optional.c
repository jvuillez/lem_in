/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cast_nbr_to_str_optional.c                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 16:57:31 by jvuillez          #+#    #+#             */
/*   Updated: 2018/01/23 16:57:33 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libprintf.h"

void		ft_print_nbr_intmax_t(t_format *f)
{
	uintmax_t	unb;
	intmax_t	nb;

	unb = (uintmax_t)f->data;
	nb = (intmax_t)f->data;
	free(f->to_print);
	if ((f->sp == 'd' || f->sp == 'D') && nb < 0)
	{
		f->sign = '-';
		f->to_print = ft_ulltoa_base(-nb, f->base);
	}
	else
		f->to_print = ft_ulltoa_base(unb, f->base);
}

void		ft_print_nbr_size_t(t_format *f)
{
	size_t	unb;
	long	nb;

	unb = (size_t)f->data;
	nb = (long)f->data;
	free(f->to_print);
	if ((f->sp == 'd' || f->sp == 'D') && nb < 0)
	{
		f->sign = '-';
		f->to_print = ft_ulltoa_base(-nb, f->base);
	}
	else
		f->to_print = ft_ulltoa_base(unb, f->base);
}
