/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/15 22:38:28 by jvuillez          #+#    #+#             */
/*   Updated: 2014/04/16 18:03:06 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *s1, const char *s2, size_t n)
{
	if (ft_strcmp(s2, "") == 0)
		return ((char*)s1);
	while ((*s1 != '\0') && (n > 0))
	{
		if (*s1 == *s2 && ft_strncmp(s1, s2, ft_strlen(s2)) == 0)
			if (ft_strlen(s2) <= (int)n)
				return ((char*)s1);
		s1++;
		n--;
	}
	return (NULL);
}
