/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_format.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/28 16:37:48 by jvuillez          #+#    #+#             */
/*   Updated: 2017/12/28 16:37:49 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libprintf.h"

/*
**	%[flags][width][.precision][length]specifier
**
**	%[flags]		-	;	+	;	~space	;	#	;	0
**	[width]			~number		;	*
**	[.precision]	~.number	;	.*
**	[length]		modify the specifier
**					hh	;	h	;	l	;	ll	;	j	;	z	;	t	;	L
**		42			hh	;	h	;	l	;	ll	;	j	;	z
** specifier	% s   p d   i o   u   x X c   f F e E g G a A n
** specifier42	% s S p d D i o O u U x X c C
*/

int				ft_flags(char *str, t_format *f)
{
	static char	flags[] = "-+ #0";
	static int	flag_value[] = {1, 2, 4, 8, 16};
	int			i;
	int			j;

	i = 0;
	j = 0;
	while (j != 5)
	{
		j = 0;
		while (j != 5 && flags[j] != str[i])
			j++;
		if (j != 5 && (f->flag & flag_value[j]) == 0)
			f->flag += flag_value[j];
		i++;
	}
	if (f->flag != 0)
		f->f = 1;
	if ((f->flag & ZERO) != 0 && (f->flag & MOINS) != 0)
		f->flag -= 16;
	return (i - 1);
}

int				ft_width(char *str, t_format *f, va_list ap)
{
	int		i;

	i = 0;
	if (str[i] >= '1' && str[i] <= '9')
	{
		f->w = 1;
		f->width = ft_atoi(&str[i]);
		i += ft_nb_length(ft_atoi(&str[i]), "0123456789");
	}
	if (str[i] == '*')
	{
		f->w = 1;
		f->width = va_arg(ap, int);
		i++;
	}
	if ((f->flag & MOINS) == 1 && f->width >= 0)
		f->width *= -1;
	if (i != 0)
		return (i + ft_width(str + i, f, ap));
	return (i);
}

int				ft_precision(char *str, t_format *f, va_list ap)
{
	int		i;

	i = 0;
	if (str[i] == '.')
	{
		f->p = 1;
		f->prec = 0;
		if ((str[++i] >= '0' && str[i] <= '9') || str[i] == '-')
		{
			f->prec = ft_atoi(&str[i]);
			if (f->prec < 0)
				i++;
			i += ft_nb_length(ft_atoi(&str[i]), "0123456789");
		}
		if (str[i] == '*')
		{
			f->prec = va_arg(ap, int);
			i++;
			if (f->prec < 0)
				f->p = 0;
			if (f->prec < 0)
				f->prec = 0;
		}
	}
	return (i);
}

int				ft_get_length(char *str, t_format *f)
{
	static char	lengths[] = "hljz";
	int			j;

	j = 0;
	if (f->prec < 0)
		f->width = 0;
	while (j != 4 && str[0] != lengths[j])
		j++;
	if (j != 4)
	{
		if (lengths[j] > f->length)
			f->length = lengths[j];
		if (str[1] == str[0])
		{
			if (f->length == 'l' || f->length == 'h')
				f->length = ft_toupper(f->length);
			return (2);
		}
		else
			return (1);
	}
	return (0);
}

/*
**	typedef struct		s_format{
**		void			*data;
**		void			(*ft_ptr)(struct s_format *f);
**		bool			f;
**		int				flag;
**		bool			w;
**		int				width;
**		bool			p;
**		int				prec;
**		int				length;
**		char			base[17];
**		char			sign;
**		int				sp;
**		int				tot;
**		char			*to_print;
**	}					t_format;
*/

char			*ft_get_format(char *str, t_format *f, va_list ap)
{
	void		*data;

	f->f = 0;
	f->flag = 0;
	f->w = 0;
	f->width = 0;
	f->p = 0;
	f->prec = 0;
	f->length = '0';
	while (ft_true_char(*str) != -1 && ft_specifier(*str) == -1)
	{
		str += ft_flags(str, f);
		str += ft_width(str, f, ap);
		str += ft_precision(str, f, ap);
		str += ft_get_length(str, f);
	}
	f->sp = (int)*str;
	f->tot = 0;
	f->to_print = ft_strnew(0);
	if (ft_specifier(f->sp) != -1 && f->sp != '%')
	{
		data = va_arg(ap, void*);
		f->data = data;
	}
	return (str);
}
