/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_unicode.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/11 19:04:44 by jvuillez          #+#    #+#             */
/*   Updated: 2017/12/11 19:04:46 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libprintf.h"

int		ft_what_the_size(wchar_t value)
{
	value = ft_size_bin(value);
	if (value > 7 && value <= 11)
		return (2);
	if (value > 11 && value <= 16)
		return (3);
	if (value > 16)
		return (4);
	return (1);
}

int		ft_n_ustrlen(wchar_t *wstr, int n)
{
	wchar_t	i;
	int		tot;

	i = 0;
	tot = 0;
	while (wstr[i] != '\0' && ft_what_the_size(wstr[i]) + tot <= n)
		tot += ft_what_the_size(wstr[i++]);
	return (tot);
}

int		ft_print_unicode(unsigned char *octet, size_t size, wchar_t value)
{
	static unsigned int		mask[4] = {0, 49280, 14712960, 4034953344};

	if (size > 16)
	{
		octet[3] = (value << 26) >> 26;
		octet[2] = ((value >> 6) << 26) >> 26;
		octet[1] = ((value >> 12) << 26) >> 26;
		octet[0] = ((value >> 18) << 29) >> 29;
		octet[0] = (mask[3] >> 24) | octet[0];
		octet[1] = ((mask[3] << 8) >> 24) | octet[1];
		octet[2] = ((mask[3] << 16) >> 24) | octet[2];
		octet[3] = ((mask[3] << 24) >> 24) | octet[3];
	}
	write(1, &octet[0], 1);
	if (size > 7)
		write(1, &octet[1], 1);
	else
		return (1);
	if (size > 11)
		write(1, &octet[2], 1);
	else
		return (2);
	if (size > 16)
		write(1, &octet[3], 1);
	return (size > 16) ? 4 : 3;
}

int		ft_putwchar(wchar_t v)
{
	static unsigned int		mask[4] = {0, 49280, 14712960, 4034953344};
	static unsigned char	octet[4] = {0, 0, 0, 0};
	unsigned int			value;

	value = v;
	if (ft_size_bin(value) <= 7)
		octet[0] = value;
	else if (ft_size_bin(value) <= 11)
	{
		octet[1] = (value << 26) >> 26;
		octet[0] = ((value >> 6) << 27) >> 27;
		octet[0] = (mask[1] >> 8) | octet[0];
		octet[1] = ((mask[1] << 24) >> 24) | octet[1];
	}
	else if (ft_size_bin(value) <= 16)
	{
		octet[2] = (value << 26) >> 26;
		octet[1] = ((value >> 6) << 26) >> 26;
		octet[0] = ((value >> 12) << 28) >> 28;
		octet[0] = (mask[2] >> 16) | octet[0];
		octet[1] = ((mask[2] << 16) >> 24) | octet[1];
		octet[2] = ((mask[2] << 24) >> 24) | octet[2];
	}
	return (ft_print_unicode(octet, ft_size_bin(value), value));
}
