/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_char.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/28 15:16:06 by jvuillez          #+#    #+#             */
/*   Updated: 2017/12/28 15:16:07 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libprintf.h"

void		ft_print_null_c(t_format *f)
{
	char to_spam;

	to_spam = ' ';
	if ((f->flag & ZERO) != 0)
		to_spam = '0';
	ft_decrease_width(f, 1);
	if (f->prec < 0)
		f->width = 0;
	if (f->width > 0)
		f->tot += f->width;
	else if (f->width < 0)
		f->tot -= f->width;
	ft_putstr_free(ft_strspam(to_spam, f->width - ft_strlen(f->to_print)));
	ft_putchar('\0');
	ft_putstr_free(ft_strspam(' ', -f->width - ft_strlen(f->to_print)));
	f->tot += \
	ft_putstr_free(ft_strspam(' ', -f->prec - ft_strlen(f->to_print) - 1));
	f->tot += 1;
}

void		ft_print_c(t_format *f)
{
	char c;
	char to_spam;

	to_spam = ' ';
	if ((f->flag & ZERO) != 0)
		to_spam = '0';
	if (f->sp == '%')
	{
		c = '%';
		f->data = &c;
	}
	else
		c = (char)f->data;
	if (f->data == '\0')
		f->ft_ptr = &ft_print_null_c;
	else
	{
		f->to_print = ft_strjoin_free(f->to_print, ft_strspam(c, 1), 2);
		ft_add_width(to_spam, ' ', f);
		if (f->prec < 0)
			ft_add_prec(' ', f);
	}
}

void		ft_print_bigc(t_format *f)
{
	char		to_spam;
	int			size;

	to_spam = ' ';
	if ((f->flag & ZERO) != 0)
		to_spam = '0';
	size = ft_what_the_size((wchar_t)f->data);
	f->tot += ft_putstr_free(ft_strspam(to_spam, f->width - size));
	f->tot += ft_putwchar((wchar_t)f->data);
	f->tot += ft_putstr_free(ft_strspam(to_spam, -f->width - size));
}

void		ft_bigc(t_format *f)
{
	wchar_t		w;

	w = (wchar_t)f->data;
	if (w <= -1 || w >= 1114111)
		f->tot = -1;
	else if (w >= 55296 && w <= 57343)
		f->tot = -1;
	else
		f->ft_ptr = &ft_print_bigc;
}
