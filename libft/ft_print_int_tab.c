/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_int_tab.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/08 16:41:00 by jvuillez          #+#    #+#             */
/*   Updated: 2018/02/08 16:41:02 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_print_int_tab(int *tab, int length)
{
	int i;

	i = 0;
	if (tab != NULL)
	{
		while (i < length)
		{
			ft_putnbr(tab[i]);
			ft_putchar(' ');
			i++;
		}
		ft_putchar('\n');
	}
}
