/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/26 14:54:45 by jvuillez          #+#    #+#             */
/*   Updated: 2018/01/26 14:54:47 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int		ft_return_error(t_lm_line *start, t_description *anthill)
{
	ft_printf("ERROR\n");
	if (anthill != NULL)
	{
		ft_free_room_list(anthill->first);
		free(anthill);
	}
	ft_free_lem_list(&start);
	return (0);
}

int		main(void)
{
	t_lm_line		*start;
	t_description	*anthill;

	start = ft_fd0_lexer();
	start = ft_remove_comment(start);
	ft_check_entry(&start);
	anthill = ft_init_anthill(start);
	if (ft_check_anhill(anthill) == -1)
		return (ft_return_error(start, anthill));
	anthill->start->nb_ants = anthill->nb_ants;
	anthill->start->ant_name = 0;
	ft_print_lines_list(start);
	ft_solver(anthill);
	ft_free_room_list(anthill->first);
	free(anthill);
	ft_free_lem_list(&start);
	return (0);
}
