/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/31 18:44:30 by jvuillez          #+#    #+#             */
/*   Updated: 2018/01/31 18:44:32 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

/*
**	ok lines :
**	NUMBER\\			-> only once and first (exept comments)
**	NAME CORDx CORDy\\	-> ft_isdigit(ft_strsplit(line, ' ')[__1 & 2__]))
**	NAME-NAME\\
**	##end\\				-> only once ?
**	##start\\			-> only once ?
**	#...
*/

void		ft_remove_next_elem(t_lm_line *start)
{
	t_lm_line *tmp;

	tmp = start->next;
	start->next = start->next->next;
	free(tmp->str);
	free(tmp);
}

t_lm_line	*ft_remove_comment(t_lm_line *first)
{
	t_lm_line	*start;
	t_lm_line	*tmp;

	tmp = first;
	start = first;
	while (tmp != NULL && ft_check_comment(tmp->str) == 1)
	{
		start = tmp->next;
		ft_printf("%s\n", tmp->str);
		free(tmp->str);
		free(tmp);
		tmp = start;
	}
	while (tmp != NULL && tmp->next != NULL)
	{
		while (tmp->next != NULL && ft_check_comment(tmp->next->str) == 1)
			ft_remove_next_elem(tmp);
		tmp = tmp->next;
	}
	return (start);
}

int			ft_free_lem_list(t_lm_line **start)
{
	t_lm_line		*tmp;
	t_lm_line		*to_free;

	tmp = *start;
	while (tmp != NULL)
	{
		to_free = tmp;
		tmp = tmp->next;
		free(to_free->str);
		free(to_free);
	}
	*start = NULL;
	return (-1);
}

void		ft_print_lines_list(t_lm_line *start)
{
	t_lm_line	*tmp;

	tmp = start;
	while (tmp != NULL)
	{
		ft_printf("%s\n", tmp->str);
		tmp = tmp->next;
	}
	ft_printf("\n");
}

t_lm_line	*ft_fd0_lexer(void)
{
	char		*line;
	t_lm_line	*start;
	t_lm_line	*tmp;

	start = NULL;
	line = NULL;
	if (get_next_line(0, &line) == 0)
		return (NULL);
	if ((start = malloc(sizeof(t_lm_line))) == NULL)
		return (NULL);
	start->str = line;
	start->next = NULL;
	tmp = start;
	while (get_next_line(0, &line) != 0)
	{
		if (line != NULL && line[0] != '\0')
		{
			if ((tmp->next = malloc(sizeof(t_lm_line))) == NULL)
				return (start);
			tmp = tmp->next;
			tmp->str = line;
			tmp->next = NULL;
		}
	}
	return (start);
}
