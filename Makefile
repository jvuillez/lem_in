# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/04/19 23:42:33 by jvuillez          #+#    #+#              #
#    Updated: 2017/12/28 18:10:43 by jvuillez         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc

CFLAGS = -Wall -Werror -Wextra

NAME = lem-in

LIBS = ./libft/libft.a

OBJ_O = main.o ft_list.o ft_check.o ft_rooms.o ft_links.o ft_anthill.o ft_solver.o

all: $(NAME)

$(NAME): $(OBJ_O)
	make -C ./libft/
	$(CC) $(CFLAGS) -o $(NAME) $(OBJ_O) $(LIBS)

%.o: %.c
	$(CC) -o $@ -c $^ $(CFLAGS)

clean:
	rm -f $(OBJ_O)
	make -C ./libft/ clean

fclean: clean
	rm -f $(NAME)
	make -C ./libft/ fclean

re: fclean all

er: all clean
