/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_solver.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/09 17:38:11 by jvuillez          #+#    #+#             */
/*   Updated: 2018/02/09 17:38:13 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int		ft_is_there_a_better_suck(t_room *empty, t_room *to_suck, int *t_o_m_a)
{
	t_lm_link	*tmp;

	if (to_suck->type == START)
		return (1);
	tmp = empty->links;
	while (tmp != NULL)
	{
		if (tmp->room->ant_name < to_suck->ant_name
			&& tmp->room->l_t_o <= to_suck->l_t_o
			&& tmp->room->type != END
			&& tmp->room->ant_name > 0
			&& t_o_m_a[tmp->room->ant_name] == 0
			&& tmp->room->ant_come_from != empty)
		{
			return (0);
		}
		tmp = tmp->next;
	}
	return (1);
}

int		ft_should_suck(
		t_room *empty, t_room *to_suck, int *t_o_m_a, t_description *anthill)
{
	if (to_suck->nb_ants == 0)
		return (1);
	if (to_suck->type == END)
		return (1);
	if (empty->type == END)
		return (0);
	if (t_o_m_a[to_suck->ant_name] == 0)
	{
		if (ft_is_there_a_better_suck(empty, to_suck, t_o_m_a) == 0)
			return (1);
		if (to_suck->l_t_o > empty->l_t_o)
			return (0);
		if (to_suck->type == START)
			return (0);
		if (to_suck->ant_come_from == empty)
			return (1);
		if (empty->l_t_o <= anthill->start->nb_ants * 2)
			return (0);
	}
	(void)anthill;
	return (1);
}

void	ft_suck(t_room *room, t_description *anthill, int *t_o_m_a)
{
	t_lm_link	*l;

	l = room->links;
	while (l != NULL)
	{
		if (room->type == END || room->nb_ants == 0)
		{
			if (ft_should_suck(room, l->room, t_o_m_a, anthill) == 0)
			{
				if (l->room->type == START)
					room->ant_name = anthill->nb_ants + 1 - l->room->nb_ants;
				else
				{
					room->ant_name = l->room->ant_name;
					l->room->ant_name = -1;
				}
				ft_printf("L%i-%s ", room->ant_name, room->name);
				l->room->nb_ants--;
				room->nb_ants++;
				room->ant_come_from = l->room;
				t_o_m_a[room->ant_name]++;
			}
		}
		l = l->next;
	}
}

void	ft_progress(t_description *anthill, int i, int *tab_of_moving_ants)
{
	t_room	*tmp;

	tmp = NULL;
	tmp = anthill->first;
	while (tmp != NULL)
	{
		if (tmp->l_t_o == i && tmp->type != START)
			ft_suck(tmp, anthill, tab_of_moving_ants);
		tmp = tmp->next_in_list;
	}
}

int		ft_solver(t_description *anthill)
{
	int		*tab_of_moving_ants;
	int		i;

	if (!(tab_of_moving_ants = malloc(sizeof(int*) * anthill->nb_ants + 1)))
		return (-1);
	ft_reset_int_tab(tab_of_moving_ants, anthill->nb_ants + 1);
	while (anthill->end->nb_ants < anthill->nb_ants)
	{
		i = -1;
		while (++i <= anthill->nb_ants + anthill->start->l_t_o)
		{
			ft_progress(anthill, i, tab_of_moving_ants);
		}
		ft_reset_int_tab(tab_of_moving_ants, anthill->nb_ants + 1);
		ft_putchar('\n');
	}
	free(tab_of_moving_ants);
	return (0);
}
