/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rooms.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/02 17:20:34 by jvuillez          #+#    #+#             */
/*   Updated: 2018/02/02 17:20:36 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void			ft_print_link_list(t_lm_link *start)
{
	t_lm_link	*tmp;

	tmp = start;
	ft_printf(" links");
	while (tmp != NULL)
	{
		ft_printf("; %p ", tmp->room);
		tmp = tmp->next;
	}
}

char			*ft_get_name(char *line)
{
	char	*name;
	int		i;

	i = 0;
	while (line[i] != ' ')
		i++;
	if (!(name = malloc(sizeof(char) * i + 1)))
		return (NULL);
	name = ft_strncpy(name, line, i);
	name[i] = '\0';
	return (name);
}

void			ft_free_room_list(t_room *first)
{
	t_room	*to_free;

	while (first != NULL)
	{
		to_free = first;
		first = first->next_in_list;
		ft_free_lem_links(&to_free->links);
		free(to_free->name);
		free(to_free);
	}
}

void			ft_print_room(t_room *room)
{
	ft_printf("%15p ", room);
	ft_printf("%c%10.10s", '\t', room->name);
	ft_printf("    type%4i", room->type);
	ft_printf("   nb_ants%4i", room->nb_ants);
	ft_printf("%20p", room->next_in_list);
	ft_printf("  l_t_o%3i", room->l_t_o);
	ft_printf("  ant_come_from%15p", room->ant_come_from);
	ft_printf("  name%4i    ", room->ant_name);
	ft_print_link_list(room->links);
	ft_printf("\n");
}

t_room			*ft_add_room(t_description *anthill, int type, char *line)
{
	t_room	*room;

	if (anthill == NULL || line == NULL)
		return (NULL);
	if (!(room = malloc(sizeof(t_room))))
		return (NULL);
	room->name = ft_get_name(line);
	room->type = type;
	room->nb_ants = 0;
	room->next_in_list = NULL;
	room->links = NULL;
	room->l_t_o = -1;
	room->ant_come_from = 0;
	room->ant_name = -1;
	if (anthill->first == NULL)
		anthill->first = room;
	if (type == START)
		anthill->start = room;
	if (type == END)
		anthill->end = room;
	return (room);
}
