/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_anthill.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/06 18:39:44 by jvuillez          #+#    #+#             */
/*   Updated: 2018/02/06 18:39:45 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void			ft_print_anthill(t_description *anthill)
{
	t_room			*room;

	room = anthill->first;
	while (room != NULL)
	{
		ft_print_room(room);
		room = room->next_in_list;
	}
}

int				ft_check_anhill(t_description *anthill)
{
	t_room		*room;
	int			nb_rooms_start;
	int			nb_rooms_end;

	if (anthill == NULL)
		return (-1);
	nb_rooms_start = 0;
	nb_rooms_end = 0;
	if (anthill->first == NULL)
		return (-1);
	room = anthill->first;
	while (room != NULL)
	{
		if (room->type == START)
			nb_rooms_start++;
		if (room->type == END)
			nb_rooms_end++;
		room = room->next_in_list;
	}
	if (nb_rooms_start != 1 || nb_rooms_end != 1)
		return (-1);
	if (anthill->start->l_t_o == -1)
		return (-1);
	return (0);
}

void			ft_get_anthill(t_lm_line *read, t_description *anthill)
{
	int				type;
	t_room			*tmp;

	while (read != NULL && ft_check_room(read->str) == 0)
	{
		type = REGULAR;
		while (ft_check_instr(read->str) >= 1)
		{
			type = ft_check_instr(read->str);
			read = read->next;
		}
		if (anthill->first == NULL)
			tmp = ft_add_room(anthill, type, read->str);
		else if (ft_check_instr(read->str) == 0)
		{
			tmp->next_in_list = ft_add_room(anthill, type, read->str);
			tmp = tmp->next_in_list;
		}
		read = read->next;
	}
	ft_create_links(read, anthill->first);
}

int				ft_pathfinding(t_room *room, int l_t_o)
{
	t_lm_link	*link;

	if (room == NULL)
		return (-1);
	link = room->links;
	if (room->l_t_o == -1 || room->l_t_o > l_t_o)
		room->l_t_o = l_t_o;
	else
		return (1);
	if (room->type == START)
		return (1);
	while (link != NULL)
	{
		ft_pathfinding(link->room, l_t_o + 1);
		link = link->next;
	}
	return (0);
}

t_description	*ft_init_anthill(t_lm_line *read)
{
	t_description	*anthill;

	if (read == NULL)
		return (NULL);
	if (!(anthill = malloc(sizeof(t_description))))
		return (NULL);
	anthill->nb_ants = ft_atoi(read->str);
	anthill->first = NULL;
	anthill->start = NULL;
	anthill->end = NULL;
	read = read->next;
	ft_get_anthill(read, anthill);
	ft_pathfinding(anthill->end, 0);
	return (anthill);
}
