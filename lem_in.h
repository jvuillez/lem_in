/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/26 15:00:12 by jvuillez          #+#    #+#             */
/*   Updated: 2018/01/26 15:00:14 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEM_IN_H
# define LEM_IN_H

# define REGULAR 0
# define START 1
# define END 2

# include "./libft/libft.h"
# include "./libft/libprintf.h"
# include "./libft/get_next_line.h"

typedef struct s_lmlink	t_lm_link;

typedef struct		s_room{
	char			*name;
	int				type;
	struct s_room	*next_in_list;
	int				l_t_o;
	int				nb_ants;
	int				ant_name;
	struct s_room	*ant_come_from;
	t_lm_link		*links;
}					t_room;

struct				s_lmlink{
	t_room			*room;
	struct s_lmlink	*next;
};

typedef struct		s_description{
	int				nb_ants;
	t_room			*first;
	t_room			*start;
	t_room			*end;
}					t_description;

typedef struct		s_lmline{
	char			*str;
	struct s_lmline	*next;
}					t_lm_line;

/*
**	ft_anthill.c
*/

void				ft_print_anthill(t_description *anthill);
int					ft_check_anhill(t_description *anthill);
void				ft_get_anthill(t_lm_line *read, t_description *anthill);
int					ft_pathfinding(t_room *room, int l_t_o);
t_description		*ft_init_anthill(t_lm_line *read);

/*
**	ft_check.c
*/

int					ft_check_comment(char *line);
int					ft_check_instr(char *line);
int					ft_check_room(char *line);
int					ft_check_link(char *line);
int					ft_check_entry(t_lm_line **start);

/*
**	ft_links.c
*/

int					ft_free_lem_links(t_lm_link **start);
void				ft_get_names_to_link(
					char *line, char **name_1, char **name_2);
void				ft_add_link(t_room *room_1, t_room *room_2, int other_way);
int					ft_add_ptr(t_room *list, char *name_1, char *name_2);
int					ft_create_links(t_lm_line *read, t_room *first);

/*
**	ft_list.c
*/

void				ft_remove_next_elem(t_lm_line *start);
t_lm_line			*ft_remove_comment(t_lm_line *first);
int					ft_free_lem_list(t_lm_line **start);
void				ft_print_lines_list(t_lm_line *start);
t_lm_line			*ft_fd0_lexer(void);

/*
**	ft_rooms.c
*/

void				ft_print_link_list(t_lm_link *start);
char				*ft_get_name(char *line);
void				ft_free_room_list(t_room *first);
void				ft_print_room(t_room *room);
t_room				*ft_add_room(t_description *anthill, int type, char *line);

/*
**	ft_solver.c
*/

int					ft_should_suck(
					t_room *empty, t_room *to_suck,
					int *t_o_m_a, t_description *anthill);
int					ft_is_there_a_better_suck(
					t_room *empty, t_room *to_suck, int *t_o_m_a);
void				ft_suck(
					t_room *room, t_description *anthill, int *t_o_m_a);
void				ft_progress(
					t_description *anthill, int i, int *tab_of_moving_ants);
int					ft_solver(
					t_description *anthill);

/*
**	main.c
*/

int					ft_return_error(t_lm_line *start, t_description *anthill);

#endif
