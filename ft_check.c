/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/01 17:11:00 by jvuillez          #+#    #+#             */
/*   Updated: 2018/02/01 17:11:01 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int		ft_check_comment(char *line)
{
	if (line == NULL)
		return (-1);
	if (line[0] == '\0')
		return (1);
	if (line[0] == '#')
		if (line[1] != '#' || ft_check_instr(line) == -2)
			return (1);
	return (0);
}

int		ft_check_instr(char *line)
{
	if (line == NULL)
		return (-1);
	if (line[0] == '#')
		if (line[1] == '#')
		{
			if (ft_strcmp(line, "##start") == 0)
				return (START);
			if (ft_strcmp(line, "##end") == 0)
				return (END);
			return (-2);
		}
	return (0);
}

int		ft_check_room(char *line)
{
	if (line[0] == '#')
		return (0);
	if (ft_count_words_whitespaces(line) == 3)
		return (0);
	return (-1);
}

int		ft_check_link(char *line)
{
	if (line[0] == '#')
		return (0);
	if (ft_count_words_whitespaces(line) == 3)
		return (1);
	if (ft_count_words(line, '-') == 2)
		return (0);
	return (-1);
}

int		ft_check_entry(t_lm_line **start)
{
	t_lm_line	*tmp;

	tmp = *start;
	while (tmp != NULL && ft_check_comment(tmp->str) == 1)
		tmp = tmp->next;
	if (tmp != NULL)
		if (ft_atoi(tmp->str) < 0)
			return (ft_free_lem_list(start));
	if (tmp != NULL)
		while (tmp->next != NULL && ft_check_room(tmp->next->str) == 0)
			tmp = tmp->next;
	if (tmp != NULL)
		while (tmp->next != NULL && ft_check_link(tmp->next->str) == 0)
			tmp = tmp->next;
	if (tmp != NULL)
		return (ft_free_lem_list(&tmp->next));
	return (0);
}
